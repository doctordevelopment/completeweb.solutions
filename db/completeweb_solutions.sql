-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2016 at 10:00 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `compweb.solutions`
--

-- --------------------------------------------------------

--
-- Table structure for table `my_admin_attempts`
--

CREATE TABLE `my_admin_attempts` (
  `maa_id` int(11) NOT NULL,
  `maa_ip_address` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `maa_attempts` int(11) NOT NULL,
  `maa_last_login_attempt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `my_admin_attempts`
--

INSERT INTO `my_admin_attempts` (`maa_id`, `maa_ip_address`, `maa_attempts`, `maa_last_login_attempt`) VALUES
(1, '::1', 0, '2016-03-29 13:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `my_categories`
--

CREATE TABLE `my_categories` (
  `mc_id` int(11) NOT NULL,
  `mc_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `mc_alias` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `mc_status` tinyint(1) NOT NULL DEFAULT '1',
  `mc_create_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `mc_update_datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mc_updated_by_idFk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `my_categories`
--

INSERT INTO `my_categories` (`mc_id`, `mc_name`, `mc_alias`, `mc_status`, `mc_create_datetime`, `mc_update_datetime`, `mc_updated_by_idFk`) VALUES
(1, 'Drupal', 'drupal', 1, '2016-03-05 14:04:12', '2016-03-05 14:04:12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `my_webadmins`
--

CREATE TABLE `my_webadmins` (
  `ma_id` int(11) NOT NULL,
  `ma_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ma_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ma_status` tinyint(1) NOT NULL,
  `ma_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ma_passwordtoken` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ma_create_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `my_webadmins`
--

INSERT INTO `my_webadmins` (`ma_id`, `ma_username`, `ma_password`, `ma_status`, `ma_name`, `ma_passwordtoken`, `ma_create_datetime`) VALUES
(1, 'admin@gmail.com', '0192023a7bbd73250516f069df18b500', 1, 'Super Admin', '', '2016-01-02 18:45:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `my_admin_attempts`
--
ALTER TABLE `my_admin_attempts`
  ADD PRIMARY KEY (`maa_id`);

--
-- Indexes for table `my_categories`
--
ALTER TABLE `my_categories`
  ADD PRIMARY KEY (`mc_id`),
  ADD KEY `mc_updated_by_idFk` (`mc_updated_by_idFk`);

--
-- Indexes for table `my_webadmins`
--
ALTER TABLE `my_webadmins`
  ADD PRIMARY KEY (`ma_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `my_admin_attempts`
--
ALTER TABLE `my_admin_attempts`
  MODIFY `maa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `my_categories`
--
ALTER TABLE `my_categories`
  MODIFY `mc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `my_webadmins`
--
ALTER TABLE `my_webadmins`
  MODIFY `ma_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
