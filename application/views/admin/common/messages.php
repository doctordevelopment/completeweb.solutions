<div class="bs-example-bg-classes">
    <?php if ($this->session->userdata('info')) { ?>
        <div class="alert alert-danger alert-info"><?= $this->session->userdata('info'); ?></div>
        <?php
        $this->session->unset_userdata('info');
    }
    ?>
    <?php if ($this->session->userdata('warning')) { ?>
        <div class="alert alert-warning alert-info"><?= $this->session->userdata('warning'); ?></div>
        <?php
        $this->session->unset_userdata('warning');
    }
    ?>
    <?php if ($this->session->userdata('error')) { ?>
        <div class="alert alert-danger alert-dismissable"><?= $this->session->userdata('error'); ?></div>
        <?php
        $this->session->unset_userdata('error');
    }
    ?> 
    <?php if ($this->session->userdata('success')) { ?>
        <div class="alert alert-success alert-dismissable"><?= $this->session->userdata('success'); ?></div>
        <?php
        $this->session->unset_userdata('success');
    } ?>
</div>
<!--success message moved to main_menu.php file-->