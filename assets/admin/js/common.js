function blockUserInputs(obj, event, integers, characters, brackets, comma, backslash, attherateof, dot, underscore, hyphen, singleinvertedcomma, ampersent, allowspace) {
    code = event.which;
    //alert(code);
    if (integers) {
        if (code >= 48 && code <= 57) {
            return true;
        }
    }

    if (characters) {
        if ((code >= 65 && code <= 90) || (code >= 97 && code <= 122)) {
            return true;
        }
    }

    if (brackets) {
        if (code >= 40 && code <= 41) {
            return true;
        }
    }

    if (comma) {
        if (code == 44) {
            return true;
        }
    }

    if (backslash) {
        if (code == 47) {
            return true;
        }
    }

    if (attherateof) {
        if (code == 64) {
            return true;
        }
    }

    if (dot) {
        if (code == 46) {
            return true;
        }
    }

    if (underscore) {
        if (code == 95) {
            return true;
        }
    }

    if (hyphen) {
        if (code == 45) {
            return true;
        }
    }

    if (singleinvertedcomma) {
        if (code == 39) {
            return true;
        }
    }

    if (ampersent) {
        if (code == 38) {
            return true;
        }
    }

    if (allowspace) {
        if (code == 32) {
            return true;
        }
    }

    code = event.keyCode;
    //alert(code);
    if (integers) {
        if (code >= 48 && code <= 57) {
            return true;
        }
    }

    if (characters) {
        if ((code >= 65 && code <= 90) || (code >= 97 && code <= 122)) {
            return true;
        }
    }

    if (brackets) {
        if (code >= 40 && code <= 41) {
            return true;
        }
    }

    if (comma) {
        if (code == 44) {
            return true;
        }
    }

    if (backslash) {
        if (code == 47) {
            return true;
        }
    }

    if (attherateof) {
        if (code == 64) {
            return true;
        }
    }

    if (dot) {
        if (code == 46) {
            return true;
        }
    }

    if (underscore) {
        if (code == 95) {
            return true;
        }
    }

    if (hyphen) {
        if (code == 45) {
            return true;
        }
    }

    if (singleinvertedcomma) {
        if (code == 39) {
            return true;
        }
    }

    if (ampersent) {
        if (code == 38) {
            return true;
        }
    }

    if (allowspace) {
        if (code == 32) {
            return true;
        }
    }

    if ($.browser.mozilla) {
        //alert(code);
        if (code == 8 || code == 46 || (code >= 35 & code <= 40) || code == 9) {
            return true;
        }
    }

    return false;
}

function blockUI(){
    $.blockUI({ message: '<h1><img src="'+$('#adminAssetsPath').val()+'/images/ajax-loader.gif" />'});
}

function check_if_form_validates(button){
    return button.closest('form.validationEngine').validationEngine('validate', {autoHidePrompt: 3000})
}

$(document).ready(function() {
    $('button.btn-circle').tooltip();

    $('.dataTableResponsive').DataTable({
            responsive: true
    });

    $('.dialogModal').on('hidden.bs.modal', function(){
        $(this).find('form')[0].reset();
    });
});