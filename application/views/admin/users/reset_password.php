<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="CompWeb.Solutions Admin Panel Reset Password Page">
        <meta name="author" content="Doctor Development">

        <title>Complete Web Solutions - Admin Panel</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= assets_url() ?>admin/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= assets_url() ?>admin/css/theme.css" rel="stylesheet">


    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Enter Your New Password</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="post" action="<?= site_url('myCompWebAdmin_Solutions/reset_password') . '?token=' . strtoupper($resetToken) ?>">
                                <fieldset>
                                    <?php $this->load->view('admin/common/messages') ?>
                                    <input type="hidden" name="reset_password" value="reset_password">
                                    <input type="hidden" name="token" value="<?= $token ?>">
                                    <div class="form-group">
                                        <input value="" id="password" class="form-control" placeholder="Password" maxlength="50" name="password" type="password" autofocus required>
                                    </div>
                                    <div class="form-group">
                                        <input value="" id="confirm_password" class="form-control" placeholder="Confirm Password" maxlength="50" name="confirm_password" type="password" required>
                                    </div>
                                    <div class="form-group">
                                        <span><a href="<?= site_url('myCompWebAdmin_Solutions/login') ?>">Need to Login?</a></span>
                                    </div>
                                    <button class="btn btn-lg btn-success btn-block">Reset</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var password = document.getElementById("password")
            , confirm_password = document.getElementById("confirm_password");

          function validatePassword(){
            if(password.value !== confirm_password.value) {
              confirm_password.setCustomValidity("Passwords Don't Match");
            } else {
              confirm_password.setCustomValidity('');
            }
          }

          password.onchange = validatePassword;
          confirm_password.onkeyup = validatePassword;
        </script>
    </body>

</html>
