<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="CompWeb.Solutions Admin Panel Forgot Password Page">
        <meta name="author" content="Doctor Development">

        <title>Complete Web Solutions - Admin Panel</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= assets_url() ?>admin/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= assets_url() ?>admin/css/theme.css" rel="stylesheet">


    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Enter Your Email Address/Username</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="post" action="<?= site_url('myCompWebAdmin_Solutions/forgot_password') ?>">
                                <fieldset>
                                    <?php $this->load->view('admin/common/messages') ?>
                                    <input type="hidden" name="forgot_password" value="forgot_password">
                                    <input type="hidden" name="token" value="<?= $token ?>">
                                    <div class="form-group">
                                        <input value="<?= $this->input->post('username') ?>" class="form-control" placeholder="Email Address/Username" maxlength="50" name="username" type="email" autofocus required>
                                    </div>
                                    <div class="form-group">
                                        <span><a href="<?= site_url('myCompWebAdmin_Solutions/login') ?>">Need to Login?</a></span>
                                    </div>
                                    <button class="btn btn-lg btn-success btn-block">Submit</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>
