<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('asset_url()')) {

    function assets_url() {
        return base_url() . 'assets/';
    }

}


if (!function_exists('logged_in')) {

    function logged_in() {
        $_ci = & get_instance();
        if ($_ci->session->userdata('ma_username')) {
            return TRUE;
        }
        return FALSE;
    }

}

if (!function_exists('set_error')) {

    function set_error($msg) {
        $_ci = & get_instance();
        $_ci->session->set_userdata('error', $msg);
    }

}

if (!function_exists('set_warning')) {

    function set_warning($msg) {
        $_ci = & get_instance();
        $_ci->session->set_userdata('warning', $msg);
    }

}

if (!function_exists('set_success')) {

    function set_success($msg) {
        $_ci = & get_instance();
        $_ci->session->set_userdata('success', $msg);
    }

}

if (!function_exists('get_attempts')) {

    function get_attempts() {
        $_ci = & get_instance();
        $attempts_data = $_ci->common_model->find('my_admin_attempts', '*', array('maa_ip_address' => $_ci->input->ip_address()));
        if ($attempts_data) {
            if ($attempts_data[0]['maa_attempts'] >= 3) {
                $seconds  = abs(strtotime(date('Y-m-d H:i:s')) - strtotime($attempts_data[0]['maa_last_login_attempt']));
                $mins = round($seconds / 60);
                if($mins >= 30){
                    clear_admin_login_attempts();
                    return 0;
                }
            }
            return $attempts_data[0]['maa_attempts'];
        }
        return 0;
    }

}

if (!function_exists('save_admin_logins_attempt')) {

    function save_admin_logins_attempt() {
        $_ci = & get_instance();
        $attempts_data = $_ci->common_model->find('my_admin_attempts', '*', array('maa_ip_address' => $_ci->input->ip_address()));
        if ($attempts_data) {
            $attempts = $attempts_data[0]['maa_attempts'] + 1;
            $_ci->common_model->update('my_admin_attempts', array('maa_attempts' => $attempts,
                'maa_last_login_attempt' => date('Y-m-d H:i:s')), array('maa_ip_address' => $_ci->input->ip_address()));
            return $attempts;
        }
        $_ci->common_model->save('my_admin_attempts', array('maa_attempts' => 1,
            'maa_ip_address' => $_ci->input->ip_address(),
            'maa_last_login_attempt' => date('Y-m-d H:i:s')
        ));
        return 1;
    }

}

if (!function_exists('clear_admin_login_attempts')) {

    function clear_admin_login_attempts() {

        $_ci = & get_instance();
        $_ci->common_model->update('my_admin_attempts', array('maa_attempts' => 0,
            'maa_last_login_attempt' => date('Y-m-d H:i:s')), array('maa_ip_address' => $_ci->input->ip_address()));
        return TRUE;
    }

}