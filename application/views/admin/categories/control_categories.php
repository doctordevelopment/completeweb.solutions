<?php $this->load->view('admin/common/header') ?>
<!-- DataTables CSS -->
<link href="<?= assets_url() ?>admin/css/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="<?= assets_url() ?>admin/css/dataTables.responsive.css" rel="stylesheet">

<link rel="stylesheet" href="<?= assets_url() ?>admin/css/validationEngine.jquery.css" type="text/css"/>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Categories/Pages</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="">Categories/Pages List</span>
                            <div class="panel-buttons">
                            	<button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#addCategoryModal" data-placement="left" title="Add New Category"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover dataTableResponsive" id="dataTables-adminusers">
                                    <thead>
                                        <tr>
                                            <th>Sr #</th>
                                            <th>Name</th>
                                            <th>Alias</th>
                                            <th>Created Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php if($categories){ ?>
	                                    	<?php foreach($categories as $key => $categoryData){ ?>
	                                        <tr class="<?= ($key%2 == 0)?'odd':'even' ?> gradeX <?= ($categoryData['mc_status'])?'':'danger' ?>">
	                                            <td><?= ($key+1) ?></td>
	                                            <td><?= $categoryData['mc_name'] ?></td>
                                                <td><?= $categoryData['mc_alias'] ?></td>
	                                            <td class="center"><?= date('d M, Y', strtotime($categoryData['mc_create_datetime'])) ?></td>
	                                            <td class="center"><?= ($categoryData['mc_status'])?'Active':'Inactive' ?></td>
	                                            <td class="center"><button type="button" class="btn btn-<?= ($categoryData['mc_status'])?'danger':'success' ?> btn-circle btn-vsmall"  data-placement="left" title="<?= ($categoryData['mc_status'])?'Inactivate':'Activate' ?> Category"><i class="fa fa-<?= ($categoryData['mc_status'])?'times':'check' ?>"></i></button></td>
	                                        </tr>
	                                        <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

<!-- Add Category Modal Start-->
<div class="modal dialogModal fade" id="addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="addCategoryModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
        <h4 class="modal-title" id="addAdminModalLabel">Add New Category/Page</h4>
      </div>
      <form class="validationEngine" id="add_category" method="post">
        <div class="modal-body">
            <div class="form-group">
              <label for="name" class="control-label">Name: <span class="required">*</span></label>
              <input type="text" maxlength="80" onkeypress="return blockUserInputs(this,event,false,true,false,false,false,false,false,false,false,false,false,true)" class="form-control validate[required,minSize[3],maxSize[80]]" id="name" name="name">
            </div>
            <div class="form-group">
              <label for="alias" class="control-label">Alias: <span class="required">*</span></label>
              <input type="text" maxlength="80" onkeypress="return blockUserInputs(this,event,true,true,false,false,false,false,false,true,false,false,false,false)" class="form-control validate[required,minSize[3],maxSize[80]]" id="alias" name="alias">
            </div>
        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-default" data-dismiss="modal">Close</a>
          <a type="button" class="btn btn-primary save_category">Save</a>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Add Category Modal Ends -->

<!-- DataTables JavaScript -->
<script src="<?= assets_url() ?>admin/js/jquery.dataTables.min.js"></script>
<script src="<?= assets_url() ?>admin/js/dataTables.bootstrap.min.js"></script>

<script src="<?= assets_url() ?>admin/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?= assets_url() ?>admin/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>

<script src="<?= assets_url() ?>admin/js/jquery.blockUI.js"></script>

<script src="<?= assets_url() ?>admin/js/common.js"></script>
<script src="<?= assets_url() ?>admin/js/categories/category.js"></script>

<?php $this->load->view('admin/common/footer') ?>
