<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="CompWeb.Solutions Admin Panel Login Page">
        <meta name="author" content="Doctor Development">

        <title>Complete Web Solutions - Admin Panel</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?= assets_url() ?>admin/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?= assets_url() ?>admin/css/theme.css" rel="stylesheet">


    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="post" action="<?= site_url('myCompWebAdmin_Solutions/login') ?>">
                                <fieldset>
                                    <?php $this->load->view('admin/common/messages') ?>
                                    <input type="hidden" name="login" value="login">
                                    <input type="hidden" name="token" value="<?= $token ?>">
                                    <div class="form-group">
                                        <input value="<?= $this->input->post('username') ?>" class="form-control" placeholder="Username" maxlength="100" name="username" type="email" autofocus required>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Password" maxlength="50" name="password" type="password" value="" required>
                                    </div>
                                    <div class="form-group">
                                        <span><a href="<?= site_url('myCompWebAdmin_Solutions/forgot_password') ?>">Forgot Password?</a></span>
                                    </div>
                                    <button class="btn btn-lg btn-success btn-block" <?= ($this->session->userdata('disable_login'))?'disabled':''; ?>>Login</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>
