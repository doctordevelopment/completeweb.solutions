    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
        <head>
            <title>Complete Web Solutions</title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta charset="UTF-8" />
            <meta name="keywords" content="COMPLETE,WEB,WEBSITE,SOLUTIONS,LEARNING,GUIDES,LESSONS,DEVELOPMENT,TRAINNING,TUTORIALS,EXAMPLES,PHP,GUIDE,LEARN,WEBSITE-BUILDING,CODEIGNITER,MYSQL,HTML,CSS,HTML5,CSS3,BOOTSTRAP,TECHNIQUES,MEDIA-QUERIES,JAVASCRIPT,XML,XHTML,INDEX,QUERIES,PARTITIONS,JQUERY,NODE.JS,LARAVEL,ZEND,ZEND-2,ORACLE-WITH-PHP,DOM,NEWS" />
            <meta name="author" content="Doctor Development" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <meta name="description" content="Helping Materials, Guides, Lessons, Tutorials and Examples for Website Development." />
            <meta property="og:title" content="Complete Web Solutions"/>
            <meta property="og:image" content="logo.png"/>
            <meta property="og:site_name" content="compweb.solutions"/>
            <meta property="og:description" content="Helping Materials, Guides, Lessons, Tutorials and Examples for Website Development."/>

            <!-- Font Awesome -->
            <link rel="stylesheet" href="css/font-awesome.min.css" />
            <link rel="stylesheet" href="fonts/HelveticaNeue/font.css" />
            <link href="style.css" rel="stylesheet" media="screen" />	
            <link href="responsive.css" rel="stylesheet" media="screen" />	

            <script src="http://code.jquery.com/jquery.js"></script>
        </head>

        <body id="scroll_top">
            <section id="header_area">

                <div class="header_top_area">
                    <div class="header_top center">
                        <div class="header_left">
                            <span class="site_date"><?= date('d M Y') ?> &#9830;</span>
                            <nav>
                                <ul id="nav">
                                    <li><a href="">Book your Add</a></li>
                                    <li><a href="">Contact Us</a></li>
                                    <li><a href="">Site Map</a></li>
                                    <li><a href="">Register/Saved</a></li>
                                    <li><a href="">Login/Favorites</a></li>
                                    <li><a href="">Profile/None</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <!-- Clean template By wpfreeware.com -->

                <div class="fix header_bottom_area">
                    <div class="header_bottom center">
                        <div class="fix logo floatleft">
                            <h1>CompWeb.Solutions</h1>
                            <p>Development no more a Dream.</p>
                        </div>
                        <div class="fix header_add floatright">
                            <img src="http://localhost/compweb.solutions/viewport_blog_template/images/adv-486-60.png"/>
                        </div>
                    </div>
                </div>

            </section>
            <section id="header_bottom_area"></section>
            <section id="content_area">
                <div class="content center">
                    <div class="main_menu">
                        <nav>
                            <ul id="nav2">
                                <li><a href="">Drupal<span>Drupal Materials</span></a></li>
                                <li><a href="">Celebrity News <span>Juicy Hollywood Gossip</span></a>
                                    <ul>
                                        <li><a href="">Home Page</a></li>
                                        <li><a href="">Blog Page</a></li>
                                        <li><a href="">Full width Page</a></li>
                                        <li><a href="">Archive Page</a></li>
                                    </ul>
                                </li>
                                <li><a href="">Technology <span>Apps, Internet & Gadgets</span></a> </li>
                                <li><a href="">Lifestyle Tips <span>Your Health & Happiness</span></a> </li>
                                <li><a href="">Entertainment <span>Movies, Music & Reviews</span></a>
                                    <ul>
                                        <li><a href="">Home Page</a></li>
                                        <li><a href="">Blog Page</a></li>
                                        <li><a href="">Full width Page</a></li>
                                        <li><a href="">Archive Page</a></li>
                                    </ul>
                                </li>
                                <li><a href="">Travel News <span>Travel news guides & Tips</span></a></li>
                            </ul>
                        </nav>
                    </div>

                    <!-- Clean template By wpfreeware.com -->