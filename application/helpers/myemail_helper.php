<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('send_reset_password_email')) {

    function send_reset_password_email($userData) {

        if (!$userData) {
            return FALSE;
        }

        $_ci = &get_instance();
        //generate random token
        $resetToken = md5(uniqid(rand(), TRUE));

        //update token in database against user
        $_ci->common_model->update('my_webadmins', array('ma_passwordtoken' => $resetToken), array('ma_id' => $userData['ma_id']));
        $emailText = get_reset_email_text($resetToken);
        
        if (!$emailText) {
            return FALSE;
        }
        
        if (send_email($userData['ma_username'], 'Reset Your Account Password', $emailText)) {
            return TRUE;
        }
        return FALSE;
    }

}

if (!function_exists('get_reset_email_text')) {

    function get_reset_email_text($resetToken = FALSE) {

        if (!$resetToken) {
            return FALSE;
        }

        $resetLink = site_url('myCompWebAdmin_Solutions/reset_password') . '?token=' . strtoupper($resetToken);

        $emailText = ""
                . "Hello Dear,"
                . "<br />"
                . "<br />"
                . "&nbsp;&nbsp;&nbsp;&nbsp;To reset your password, click this link.<br /> "
                . "$resetLink"
                . "<br />"
                . "If you cannot access this link, copy and paste the entire URL into your browser."
                . "<br />"
                . "Complete Web Solutions Team";
        return $emailText;
    }

}

if (!function_exists('send_email')) {

    function send_email($to = FALSE, $subject = FALSE, $messageBody = FALSE) {
        
        if (!$subject) {
            $subject = 'Important';
        }

        if (!$to || !$messageBody) {
            return FALSE;
        }
        $_ci = &get_instance();
        $_ci->load->library('email');


        $config = array();
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_user'] = 'plan9.pitb@gmail.com';
        $config['smtp_pass'] = 'cargoofpia';
        $config['smtp_port'] = 465;
        $config['wordwrap'] = FALSE;
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $_ci->email->clear();
        $_ci->email->initialize($config);
        $_ci->email->from('no-reply@completewebsolutions.com');

        $_ci->email->to($to);

        if ($subject) {
            $_ci->email->subject($subject);
        }
        $_ci->email->message($messageBody);
        $_ci->email->send(FALSE);
        $_ci->email->print_debugger(array('headers'));
        
        return true;
    }

}

