</div>
    <!-- /#wrapper -->

    <!-- Custom CSS -->
    <link href="<?= assets_url() ?>admin/css/sb-admin-2.css" rel="stylesheet">

        <!-- My CSS -->
    <link href="<?= assets_url() ?>admin/css/my_css.css" rel="stylesheet">

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= assets_url() ?>admin/js/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?= assets_url() ?>admin/js/sb-admin-2.js"></script>

</body>

</html>