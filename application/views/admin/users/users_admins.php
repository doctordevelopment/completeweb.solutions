<?php $this->load->view('admin/common/header') ?>
<!-- DataTables CSS -->
<link href="<?= assets_url() ?>admin/css/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="<?= assets_url() ?>admin/css/dataTables.responsive.css" rel="stylesheet">

<link rel="stylesheet" href="<?= assets_url() ?>admin/css/validationEngine.jquery.css" type="text/css"/>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Administrators</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="">Admin Users</span>
                            <div class="panel-buttons">
                            	<button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#addAdminModal" data-placement="left" title="Add New Admin"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover dataTableResponsive" id="dataTables-adminusers">
                                    <thead>
                                        <tr>
                                            <th>Sr #</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Created Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php if($users){ ?>
	                                    	<?php foreach($users as $key => $userData){ ?>
	                                        <tr class="<?= ($key%2 == 0)?'odd':'even' ?> gradeX <?= ($userData['ma_status'])?'':'danger' ?>">
	                                            <td><?= ($key+1) ?></td>
	                                            <td><?= $userData['ma_name'] ?></td>
	                                            <td><?= $userData['ma_username'] ?></td>
	                                            <td class="center"><?= date('d M, Y', strtotime($userData['ma_create_datetime'])) ?></td>
	                                            <td class="center"><?= ($userData['ma_status'])?'Active':'Inactive' ?></td>
	                                            <td class="center"><button type="button" class="btn btn-<?= ($userData['ma_status'])?'danger':'success' ?> btn-circle btn-vsmall"  data-placement="left" title="<?= ($userData['ma_status'])?'Inactivate':'Activate' ?> Admin"><i class="fa fa-<?= ($userData['ma_status'])?'times':'check' ?>"></i></button></td>
	                                        </tr>
	                                        <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#emailModal" data-whatever="@mdo">Open modal for @mdo</a>

<!-- Add Admin Modal Start-->
<div class="modal dialogModal fade" id="addAdminModal" tabindex="-1" role="dialog" aria-labelledby="addAdminModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
        <h4 class="modal-title" id="addAdminModalLabel">Add New Admin</h4>
      </div>
      <form class="validationEngine">
        <div class="modal-body">
            <div class="form-group">
              <label for="name" class="control-label">Name: <span class="required">*</span></label>
              <input type="text" maxlength="100" onkeypress="return blockUserInputs(this,event,false,true,false,false,false,false,false,false,false,false,false,true)" class="form-control validate[required,minSize[3],maxSize[100]]" id="name" name="name">
            </div>
            <div class="form-group">
              <label for="email" class="control-label">Email: <span class="required">*</span></label>
              <input type="email" maxlength="100" class="form-control validate[required,custom[email],minSize[6],maxSize[100]]" id="email" name="email">
            </div>
        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-default" data-dismiss="modal">Close</a>
          <a type="button" class="btn btn-primary save_admin">Save</a>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Add Admin Modal Ends -->

<!-- Email Modal Start-->
<div class="modal fade dialogModal" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="emailModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="emailModalLabel">New message</h4>
      </div>
      <div class="modal-body">
        <form name="addAdminForm" class="validationEngine" id="addAdminForm">
          <div class="form-group">
            <label for="recipient-name" class="control-label">Recipient:</label>
            <input type="text" class="form-control validate[required,custom[email]]" name="recipient_name" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="email-subject" class="control-label">Subject:</label>
            <input type="text" class="form-control validate[required]" id="recipient-subject" name="recipient_subject">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label validate[required]">Message:</label>
            <textarea class="form-control" name="message_text" id="message-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-default" data-dismiss="modal">Close</a>
        <a type="button" class="btn btn-primary">Send message</a>
      </div>
    </div>
  </div>
</div>
<!-- Email Modal Ends -->
<!-- DataTables JavaScript -->
<script src="<?= assets_url() ?>admin/js/jquery.dataTables.min.js"></script>
<script src="<?= assets_url() ?>admin/js/dataTables.bootstrap.min.js"></script>

<script src="<?= assets_url() ?>admin/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?= assets_url() ?>admin/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>

<script src="<?= assets_url() ?>admin/js/jquery.blockUI.js"></script>

<script src="<?= assets_url() ?>admin/js/common.js"></script>
<script src="<?= assets_url() ?>admin/js/users/admins.js"></script>

<?php $this->load->view('admin/common/footer') ?>
