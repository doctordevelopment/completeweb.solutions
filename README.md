# Install GIT #

* Hit Clone in left menu
* Copy and right click on folder and select Git Bash Here
* Paste and hit enter 
* cd completeweb.solutions to get into folder
* git checkout dev (development will be in this branch)

**NOTE**: No one should commit in master branch admins will take care of merging code from dev to master branch.

# Admin Credentials #

**URL:** http://localhost/completeweb.solutions/myCompWebAdmin_Solutions

**Username:** admin@gmail.com

**Password:** admin123