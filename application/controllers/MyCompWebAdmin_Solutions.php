<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MyCompWebAdmin_Solutions extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('common_model');
    }

    public function index() {
        redirect(site_url('myCompWebAdmin_Solutions/login'));
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(site_url('myCompWebAdmin_Solutions/login'));
    }

    public function reset_password() {

        $viewData = array();
        $userData = array();

        $this->load->helper('security');
        $viewData['resetToken'] = $resetToken = $this->input->get('token', true);

        if ($resetToken) {
            $userData = $this->common_model->find('my_webadmins', '*', array('ma_status' => 1, 'ma_passwordtoken' => strtolower($resetToken)));
            if (!$userData) {
                set_error('Invalid/Expired Token Submitted');
                redirect(site_url('myCompWebAdmin_Solutions/login'));
            }
        }

        if ($this->input->post('reset_password')) {//form is posted
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[50]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|max_length[50]|matches[password]');
            
            if ($this->form_validation->run() == TRUE && ($this->input->post('token') == $this->session->userdata('token'))) {//form validates now check in database
                $this->common_model->update('my_webadmins', array('ma_password' => md5($this->input->post('password', TRUE)), 'ma_passwordtoken' => ''), array('ma_id' => $userData[0]['ma_id']));
                set_success('You have successfully reset your password. Please LogIn with new password.');
                redirect(site_url('myCompWebAdmin_Solutions/login'));
            } else {//form not validated
                set_error('Password and Confirm Password are required.');
            }
        }

        $viewData['token'] = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $viewData['token']);
        $this->load->view('admin/users/reset_password', $viewData);
    }

    public function forgot_password() {

        $viewData = array();

        if (logged_in()) {//if already logged in
            redirect(site_url('myCompWebAdmin_Solutions/dashboard'));
        }

        if ($this->input->post('forgot_password')) {//form is posted
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->helper('security');

            $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[100]|valid_email');

            if ($this->form_validation->run() == TRUE && ($this->input->post('token') == $this->session->userdata('token'))) {//form validates now check in database
                $userArray = array();
                $userArray['ma_username'] = $this->input->post('username', TRUE);
                $userArray['ma_status'] = 1;

                $userData = $this->common_model->find('my_webadmins', '*', $userArray);

                if (!$userData) { //username do not match
                    $message = 'Email Address/Username does not exists OR is inactiveted by admin.';
                    set_error($message);
                } else { //username found in database
                    $this->load->helper('myemail');
                    if (send_reset_password_email($userData[0])) {
                        set_success('We have sent you an email. Follow steps to reset your password. Do check your spam if email not found in your inbox.');
                        redirect(site_url('myCompWebAdmin_Solutions/login'));
                    } else {
                        set_error('Email not sent. Please try again later.');
                    }
                }
            } else {//form not validated
                set_error('Email Address/Username is required.');
            }
        }

        $viewData['token'] = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $viewData['token']);
        $this->load->view('admin/users/forgot_password', $viewData);
    }

    public function login() {

        $viewData = array();

        if (logged_in()) {//if already logged in
            redirect(site_url('myCompWebAdmin_Solutions/dashboard'));
        }

        $attempts = get_attempts(); //get admin attempts from database
        if ($attempts >= 3) {// blocks user if attempts are >= 3
            $this->session->set_userdata('disable_login', true);
            set_error('You have exceeded your login attempts try agian in 30 minutes.');
        } else {
            $this->session->set_userdata('disable_login', false);
        }

        if ($this->input->post('login')) {//form is posted
            save_admin_logins_attempt(); // save admin login attempt into database
            if ($attempts >= 3) {// blocks user if attempts are >= 3
                $this->session->set_userdata('disable_login', true);
                set_error('You have exceeded your login attempts try agian in 30 minutes.');
            } else {
                $this->session->set_userdata('disable_login', false);
                if ($attempts == 1) {// warns user if attempts are == 2 this is 1 because we fetch attempts first before save 
                    set_warning('This is your last chance to login. Incorrect username/password will result in your Ip block for 30 minutes.');
                }
                $this->load->helper('form');
                $this->load->library('form_validation');
                $this->load->helper('security');

                $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[100]|valid_email');
                $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[50]|md5');

                if ($this->form_validation->run() == TRUE && ($this->input->post('token') == $this->session->userdata('token'))) {//form validates now check in database
                    $userArray = array();
                    $userArray['ma_username'] = $this->input->post('username', TRUE);
                    $userArray['ma_password'] = $this->input->post('password', TRUE);
                    $userArray['ma_status'] = 1;
					
                    $userData = $this->common_model->find('my_webadmins', '*', $userArray);

                    if (!$userData) { //username and password do not match
                        $message = 'Username and Password do not match OR is inactiveted by admin.';
                        if ($attempts == 2) {// blocks user if attempt no is 3 this is 2 beacuse we fetch attempt before save
                            $this->session->set_userdata('disable_login', true);
                            $message .= ' You have exceeded your login attempts try agian in 30 minutes.';
                        }
                        set_error($message);
                    } else { //username found in database
                        clear_admin_login_attempts();
                        $this->session->set_userdata($userData[0]);
                        set_success('Welcome ' . $userData[0]['ma_name']);
                        redirect(site_url('myCompWebAdmin_Solutions/dashboard'));
                    }
                } else {//form not validated
                    set_error('Username and Password are required.');
                }
            }
        }
        $viewData['token'] = md5(uniqid(rand(), true));
        $this->session->set_userdata('token', $viewData['token']);
        $this->load->view('admin/users/login', $viewData);
    }

    public function dashboard() {

        if (!logged_in()) {//if not logged in
            redirect(site_url('myCompWebAdmin_Solutions/login'));
        }

        $viewData = array();
        $this->load->view('admin/dashboard', $viewData);
    }

    public function control_pages(){
        
    }

    public function control_categories(){
        if (!logged_in()) {//if not logged in
            redirect(site_url('myCompWebAdmin_Solutions/login'));
        }

        if($this->session->userdata('ma_username') != 'doctor.development@gmail.com'){
            set_error('You cannot access this module');
            redirect(site_url('myCompWebAdmin_Solutions/dashboard'));
        }

        if($this->input->post('name') && $this->input->post('alias')){
            $this->common_model->save('my_categories', array('mc_name' => $this->input->post('name'), 'mc_alias' => $this->input->post('alias')));
            set_success('Category added successfully');
            redirect(site_url('myCompWebAdmin_Solutions/control_categories'));
        }

        $viewData = array();
        $viewData['categories'] = $this->common_model->find('my_categories', '*', array(1 => 1), false, false, false, 'mc_name ASC');
        $this->load->view('admin/categories/control_categories', $viewData);
    }

    public function users_admins() {

        if (!logged_in()) {//if not logged in
            redirect(site_url('myCompWebAdmin_Solutions/login'));
        }
        
        if($this->session->userdata('ma_username') != 'doctor.development@gmail.com'){
            set_error('You cannot access this module');
            redirect(site_url('myCompWebAdmin_Solutions/dashboard'));
        }

        $viewData = array();
        $viewData['users'] = $this->common_model->find('my_webadmins', '*', array('ma_id != ' => 1), false, false, false, 'ma_name ASC');
        $this->load->view('admin/users/users_admins', $viewData);
    }

    public function users_web(){
        if (!logged_in()) {//if not logged in
            redirect(site_url('myCompWebAdmin_Solutions/login'));
        }

        if($this->session->userdata('ma_username') != 'doctor.development@gmail.com'){
            set_error('You cannot access this module');
            redirect(site_url('myCompWebAdmin_Solutions/dashboard'));
        }

        $viewData = array();
        $this->load->view('admin/users/users_web', $viewData);
    }

    public function admin_profile(){
        if (!logged_in()) {//if not logged in
            redirect(site_url('myCompWebAdmin_Solutions/login'));
        }

        if($this->session->userdata('ma_username') != 'doctor.development@gmail.com'){
            set_error('You cannot access this module');
            redirect(site_url('myCompWebAdmin_Solutions/dashboard'));
        }

        $viewData = array();
        $this->load->view('admin/users/users_web', $viewData);
    }

}
