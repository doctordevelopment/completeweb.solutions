<section id="footer_top_area">
    <div class="fix footer_top center">
        <div class="fix footer_top_container">
            <div class="fix single_footer_top floatleft">
                <h2><span>Recent posts</span></h2>
                <ul>
                    <li><a href="">single footer recent post</a> (10)</li>
                    <li><a href="">single footer recent post</a> (10)</li>
                    <li><a href="">single footer recent post</a> (10)</li>
                    <li><a href="">single footer recent post</a> (10)</li>
                    <li><a href="">single footer recent post</a> (10)</li>
                </ul>
            </div>
            <div class="fix single_footer_top floatleft">
                <h2><span>About Viewport</span></h2>
                <p>onec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
                <p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.
                    Donec id elit non mi porta gravida at eget metus.</p>

            </div>
            <div class="fix single_footer_top floatleft">
                <h2><span>Comments</span></h2>
                <ul>
                    <li>Orman Clark on <a href="">Sample Post </a>With<a href=""> Threaded Comments</a></li>
                    <li>Orman Clark on <a href="">Sample Post </a>With<a href=""> Threaded Comments</a></li>
                    <li>Orman Clark on <a href="">Sample Post </a>With<a href=""> Threaded Comments</a></li>
                </ul>
            </div>

            <div class="fix single_footer_top footer_logo floatright">
                <h1>Viewport</h1>
                <p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer.Donec id elit non mi porta gravida at eget metus.  
                </p>
            </div>

        </div>


    </div>
</section>
<section id="footer_bottom_area">
    <div class="fix footer_bottom center">
        <div class="fix copyright floatleft">
            <p>All Rights Reserved</p>
        </div>
        <div class="fix footer_bottom_text floatright">
            <p>Blog by  <a href="#">Doctor Development</a></p>
        </div>
    </div>
</section>

<script type="text/javascript" src="js/selectnav.min.js"></script>
<script type="text/javascript">
    selectnav('nav', {
        label: '-Navigation-',
        nested: true,
        indent: '-'
    });
    selectnav('nav2', {
        label: '-Navigation-',
        nested: true,
        indent: '-'
    });


</script>	
<script type="text/javascript">

    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
</script>	

</body>
</html>
